package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeDto;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {


    @Autowired
    private EmployeeRepository employeeRepository;

    public List<EmployeeDto> findAll() {
        return employeeRepository.findAll().stream()
                .map(EmployeeDto::new)
                .collect(Collectors.toList());
    }

    public EmployeeDto findById(Long id) {
        return employeeRepository.findById(id)
                .map(EmployeeDto::new)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, Employee employee) {
        employeeRepository.findById(id)
                .map(toBeUpdatedEmployee -> {
                    if (employee.getSalary() != null) {
                        toBeUpdatedEmployee.setSalary(employee.getSalary());
                    }
                    if (employee.getAge() != null) {
                        toBeUpdatedEmployee.setAge(employee.getAge());
                    }
                    employeeRepository.save(toBeUpdatedEmployee);
                    return toBeUpdatedEmployee;
                })
                .orElseThrow(EmployeeNotFoundException::new);

    }

    public List<EmployeeDto> findAllByGender(String gender) {
        return employeeRepository.findByGender(gender).stream()
                .map(EmployeeDto::new)
                .collect(Collectors.toList());
    }

    public EmployeeDto create(Employee employee) {
        Employee save = employeeRepository.save(employee);
        return new EmployeeDto(save);
    }

    public List<EmployeeDto> findByPage(Integer page, Integer size) {
        return employeeRepository.findAll(PageRequest.of(page - 1, size)).get()
                .map(EmployeeDto::new)
                .collect(Collectors.toList());
    }


    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }

}
