package com.afs.restapi.controller;

import com.afs.restapi.dto.CompanyDto;
import com.afs.restapi.dto.EmployeeDto;
import com.afs.restapi.entity.Company;
import com.afs.restapi.service.CompanyService;
import com.afs.restapi.entity.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("companies")
@RestController
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public List<CompanyDto> getAllCompanies() {
        return companyService.findAll();
    }

    @GetMapping(params = {"page", "size"})
    public List<CompanyDto> getCompaniesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        return companyService.findByPage(page, size);
    }

    @GetMapping("/{id}")
    public CompanyDto getCompanyById(@PathVariable Long id) {
        return companyService.findById(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCompany(@PathVariable Long id, @RequestBody Company company) {
        companyService.update(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyService.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyDto createCompany(@RequestBody Company company) {
        return companyService.create(company);
    }

    @GetMapping("/{id}/employees")
    public List<EmployeeDto> getEmployeesByCompanyId(@PathVariable Long id) {
        return companyService.findEmployeesByCompanyId(id);
    }

}
