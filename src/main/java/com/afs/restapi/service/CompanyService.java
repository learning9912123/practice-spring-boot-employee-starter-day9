package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyDto;
import com.afs.restapi.dto.EmployeeDto;
import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private CompanyRepository companyRepository;



    public List<CompanyDto> findAll() {
        return companyRepository.findAll().stream()
                .map(CompanyDto::new)
                .collect(Collectors.toList());
    }

    public List<CompanyDto> findByPage(Integer page, Integer size) {
        return companyRepository.findAll(PageRequest.of(page - 1, size)).toList().stream()
                .map(CompanyDto::new)
                .collect(Collectors.toList());
    }

    public CompanyDto findById(Long id) {
        return companyRepository.findById(id)
                .map(CompanyDto::new)
                .orElseThrow(CompanyNotFoundException::new);
    }

    public void update(Long id, Company company) {
        companyRepository.findById(id)
                .ifPresent(companyToUpdate -> {
                    companyToUpdate.setName(company.getName());
                    companyRepository.save(companyToUpdate);
                });
    }

    public CompanyDto create(Company company) {
        Company save = companyRepository.save(company);
        return new CompanyDto(save);
    }

    public List<EmployeeDto> findEmployeesByCompanyId(Long id) {
        return employeeRepository.findByCompanyId(id).stream()
                .map(EmployeeDto::new)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        companyRepository.deleteById(id);
    }
}
