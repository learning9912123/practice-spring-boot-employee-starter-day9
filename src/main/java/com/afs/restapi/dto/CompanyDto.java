package com.afs.restapi.dto;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.List;

public class CompanyDto {

    private Long id;

    private String name;

    private List<Employee> employees;

    public CompanyDto() {
    }

    public CompanyDto(Company company) {
        BeanUtils.copyProperties(company,this);
    }

    public CompanyDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
